#include "spdk/stdinc.h"
#include "spdk/ioat.h"
#include "spdk/env.h"
#include "spdk/queue.h"
#include "spdk/string.h"
#include "spdk/barrier.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <emmintrin.h>

struct user_config {
	int xfer_size_bytes;
	int queue_depth;
	int time_in_sec;
	bool verify;
	char *core_mask;
	int ioat_chan_num;
}user_config;
struct spdk_ioat_chan *chan1=NULL,*chan2=NULL;
void parse_args(int argc, char **argv, struct spdk_env_opts *opts)
{
	int op;
	while ((op = getopt(argc, argv, "c:hn:o:q:t:v")) != -1)
	{
		switch (op)
		{
		case 'o':
			user_config.xfer_size_bytes = spdk_strtol(optarg, 10);
			break;
		// case 'n':
		// 	user_config.chan_num = spdk_strtol(optarg, 10);
		// 	break;
		case 'q':
			// user_config.queue_depth = spdk_strtol(optarg, 10);
			break;
		case 't':
			// user_config.time_in_sec = spdk_strtol(optarg, 10);
			break;
		case 'c':
			opts->core_mask = optarg;
			break;
		case 'v':
			// user_config.verify = true;
			break;
			// case 'chan_id':
			// 	user_config.chan_id= spdk_strtol(optarg, 10);
		}
	}
	return 0;
}

struct worker
{
	uint64_t *src;
	uint64_t *dst;
	uint64_t size;
	int chan_num;
	int worker_count;
};
int count = 0;

void cb(void *arg)
{
	bool *p = (bool *)arg;
	*p = true;
	// printf("cb!\n");
}
static bool
probe_cb(void *cb_ctx, struct spdk_pci_device *pci_dev)
{
	return true;
}

static void
attach_cb(void *cb_ctx, struct spdk_pci_device *pci_dev, struct spdk_ioat_chan *ioat)
{
	if(!chan1)
	{
		chan1 = ioat;
		return;
	}

		
	printf("attach!\n");
}
struct result
{
	uint64_t time;
	double speed;
};
void display_header()
{
	printf("\n");
	printf("|---------------|-------------|---------------|--------------|--------------|--------------|--------------|\n");
	printf("| Copy          | Memcpy                      | SPDK (one channel)          | SPDK (two channel)          |\n");
	printf("| size          | Time        |   Speed       | Time         |Speed         | Time         |Speed         |\n");
	printf("|---------------|-------------|---------------|--------------|--------------|--------------|--------------|\n");
}

#define TIME 100000000
#define TIME_SEC 1
#define NUMA_NUM 4
#define LOCAL_NUMA_NUM 0
#define REMOTE_LOCAL_NUMA_NUM 1
static int
opts_init_env(int argc, char **argv)
{
	struct spdk_env_opts *opts = malloc(sizeof(struct spdk_env_opts));
	spdk_env_opts_init(opts);
	parse_args(argc, argv, opts);
	opts->name = "test";
	// opts->core_mask="0x1";
	if (spdk_env_init(opts) < 0)
		return -1;
	if (spdk_ioat_probe(NULL, probe_cb, attach_cb) != 0)
	{
		printf(stderr, "ioat_probe() failed\n");
		return -1;
	}
	// printf("here\n");
	return 0;
}

int test[] = {64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536};
int N = 1;
int main(int argc, char **argv)
{
	if (opts_init_env(argc, argv) < 0)
	{
		printf("opts init environment fail.\n");
		return 0;
	}
	else
		printf("opts init environment success.\n");
	bool flag;
	int current_socket = spdk_env_get_socket_id(spdk_env_get_current_core());
	clock_t start, finish, middle;
	double speed, duration_total, duration_overlap;
	struct cookie cookie;
	for (int i = 0; i < sizeof(test)/sizeof(test[0]); i++)
	{
    	uint64_t size = pow(2, 10) * test[i];
		int *src = spdk_malloc(size, pow(2,21) , NULL, current_socket, SPDK_MALLOC_DMA);
		int *dst = spdk_malloc(size, pow(2,21) , NULL, current_socket +1, SPDK_MALLOC_DMA);
	   
		memset(dst, 2, size);
		memset(src, 1, size);
		if (!src||!dst)
		{
			printf("src or dst is null!\n");
			exit(1);
		}
		
		flag = false;
		duration_total = 0;
		duration_overlap = 0;
		for (int j = 0; j < N; j++)
		{			

			start = clock();
			// ioat_fill(dst, 1, size, &cookie);
			// ioat_copy(dst, src, size, &cookie);
			// memset(dst, 1, size);
			memcpy(dst, src, size);
			// spdk_ioat_submit_copy(chan, &flag, cb, dst, src, size);
			// spdk_ioat_submit_fill(chan, &flag, cb, dst, 1, size);
			middle = clock();
			// while(flag == false)spdk_ioat_process_events(chan);
			// ioat_cookie_wait(&cookie);
			// sleep(2);
			// memcpy(dst, src,size);
			// while(memcmp(dst, src, size));

			// memcpy(dst, src,size);
			finish = clock();

			duration_overlap =+  (double)(finish - middle) / CLOCKS_PER_SEC;
			duration_total =+ (double)(finish - start) / CLOCKS_PER_SEC;
		}
		// spdk_free(src);
		// spdk_free(dst);
		// printf( "%f seconds\n", duration );
		
		speed = (size/ pow(2,20))*N / duration_total;
		printf("%d KiB I/OAT:total duration: %lf, overlap duration: %lf\n",test[i], duration_total, duration_overlap);
		printf("overlap %f, speed: %lf MiB/s\n", duration_overlap/duration_total, test[i],speed);

		// start = clock();
		// 
		// finish = clock();
		// // printf("here\n");
		// duration = (double)(finish - start) / CLOCKS_PER_SEC;
		// speed = (size/ pow(2,20)) / duration;
		// printf("%d KiB CPU: speed: %lf MiB/s\n",i,speed);
	}


}
