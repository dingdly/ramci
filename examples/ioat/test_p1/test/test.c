#include <stdio.h>
#include <string.h>
#include <time.h> 
#include <math.h>
#include <stdlib.h>
int main()
{ 

        char str[]="hello world";
        clock_t start_t,finish_t;
        double total_t = 0;
        char *src;
        int size=pow(2,30);
        src=(char *)malloc(sizeof(char)*size);
        char *dst;
        dst=(char *)malloc(sizeof(char)*size);
 //       strcpy(src,str);
        start_t = clock();
	memcpy(dst, src, size);
        finish_t = clock();
        total_t = (double)(finish_t - start_t) / CLOCKS_PER_SEC;//将时间转换为秒
        printf("CPU 占用的总时间：%f\n", total_t);
//	printf("%s\n",dst);
        return 0;
}
