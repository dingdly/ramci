#include "spdk/stdinc.h"
#include "spdk/ioat.h"
#include "spdk/env.h"
#include "spdk/queue.h"
#include "spdk/string.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "display_results.c"
//#include <sys/time.h>
uint64_t count=0;
uint64_t int_pow(uint64_t base, uint8_t exp)
{
    uint64_t value = 1;
    for (int i = 0; i < exp; ++i)
        value *= base;
    return value;
}
struct ioat_task
{
	uint64_t *src;
	uint64_t *dst;
	uint64_t size; 
};
static void done(void *arg)
{
	struct ioat_task *temp=(struct ioat_task*)arg;
	
	// if(memcmp((void *)temp->src, (void *)temp->dst, temp->size)==0)
	// {
			++count;
			// printf("success!%d\n",count-1);
	//	printf("%lx %lx\n",*(uint64_t *)(temp->src),*(uint64_t *)(temp->dst));
	// }
	// else printf("fail \n");

}
struct element par_spdk(uint64_t chunk_size,uint64_t buffer_size,struct spdk_ioat_chan* chan)
{
	uint64_t chunk_num=buffer_size/chunk_size;
	printf("chunk_num=%8u,chunk_size=%8u,buffer_size=%8u\n",chunk_num,chunk_size,buffer_size);
	// int src[64],dst[64];
	uint64_t *src_num=spdk_malloc(sizeof(uint64_t)*chunk_num,0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	uint64_t *dst_num=spdk_malloc(sizeof(uint64_t)*chunk_num,0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	// uint64_t *src_num=malloc(sizeof(uint64_t)*chunk_num);//会挂机
	// uint64_t *dst_num=malloc(sizeof(uint64_t)*chunk_num);//会挂机
	uint8_t *src=spdk_malloc(buffer_size,sizeof(uint64_t), NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	uint8_t *dst=spdk_malloc(buffer_size,sizeof(uint64_t), NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	printf("src=%lx~%lx,dst=%lx~%lx\n",src,src+buffer_size,dst,dst+buffer_size);
	struct ioat_task *ioat_task=spdk_malloc(sizeof(struct ioat_task)*chunk_num, 0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	uint64_t i,n=0;
	struct result temp;
	srand((unsigned) time(NULL));
	
	// printf("\n");
	srand(time(0));
	for(i=0;i<chunk_num;i++)
	{
		src_num[i]=i*chunk_size;
		dst_num[i]=(chunk_num-1-i)*chunk_size;
		// src_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
		// dst_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
		printf("%8u %8u %8u\n",src_num[i],dst_num[i],i);  //(rand()%chunk_num)
	}
	printf("1\n");

  for(i=0;i<chunk_num;i++)
	{
		ioat_task[i].src=src + src_num[i];
		ioat_task[i].dst=dst + dst_num[i];
		ioat_task[i].size=chunk_size;		
		// printf("%lx %lx\n",ioat_task[i].src,ioat_task[i].dst);
		// printf("%lx %lx\n",*ioat_task[i].src,*ioat_task[i].dst);
	}
		

	printf("2\n");
	struct timespec start,end; 
	uint64_t timeuse;
	
 	do
	{
		count=0;
		for(i=0;i<buffer_size/sizeof(uint8_t);i++)
	{
		src[i]=rand();
		dst[i]=rand();		
		// printf("%lx %lx\n",src[i],dst[i]);
	}	
		clock_gettime(CLOCK_REALTIME, &start);
		for (int i = 0; i < chunk_num; ++i)
			{
     	 // Submit 1 copy
        spdk_ioat_submit_copy(
        chan,
         &ioat_task[i],
        done,
        ioat_task[i].dst,
        ioat_task[i].src,
        ioat_task[i].size);
  		}
    do
    {
    	spdk_ioat_process_events(chan);
    } while (count!=chunk_num);
	n++;
	clock_gettime(CLOCK_REALTIME, &end);
	timeuse=timeuse+(end.tv_sec*int_pow(10,9)+end.tv_nsec)-(start.tv_sec*int_pow(10,9)+start.tv_nsec);
//	printf("timeuse.tv_usec=%luns\n",end.tv_sec);
	printf("timeuse=%luns\n",timeuse);
	}while(timeuse<1000000000);
	
	// printf("start %llus/%lluns %lluns\n",start.tv_sec,start.tv_sec*int_pow(10,9),start.tv_nsec);
	// printf("end %llus/%lluns %lluns\n",end.tv_sec,end.tv_sec*int_pow(10,9),end.tv_nsec);
	
	// printf("SPEED=%lfMib/s\n",((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9)));
	
	spdk_free(src);
	spdk_free(dst);
	spdk_free(src_num);
	spdk_free(dst_num);
	temp.time=timeuse/(n*chunk_num);
	temp.speed=((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9));
	return  temp;
	
// 		if((n=spdk_ioat_submit_copy(chan,arg,done,dst,src,sizeof(int)*chunk_num))!=0)
// 		{
// 			printf("error %d\n",n);
// 			exit(0);
// 		}	
// 	tsc_end = spdk_get_ticks() + spdk_get_ticks_hz();
// 	while(*arg!=true)
// 	{
// 		spdk_ioat_process_events(chan);
// //		printf("polling\n");
// 	}
// 	printf("\n");

	// for(i=0;i<chunk_num;i++)
	// 	{
	// 		printf("%4d ",dst[i]);
	// 		if((i+1)%32==0)printf("\n");
	// 	}
}
