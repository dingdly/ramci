
#include "spdk/ioat.h"

// In this benchmark, we're copying random chunks of memory inside a buffer. We are
// submitting n copy jobs at the same time. n is buffer_size / chunk_size / 2.
// - buffer_size is the size of the full buffer
// - chunk_size is the size of the chunks being copied around
// The buffer is initially populated with random data. To avoid ending up copying the same data over
// and over, even-indexed chunks are used as sources, and odd-indexed chunks are used as
// destination.
void par_spdk(uint64_t chunk_size, uint64_t buffer_size,struct spdk_ioat_chan* chan);


