#include "spdk/stdinc.h"
#include "spdk/ioat.h"
#include "spdk/env.h"
#include "spdk/queue.h"
#include "spdk/string.h"
#include <stdlib.h>
#define ELE_NUM 23
struct channel {
	struct spdk_ioat_chan *ioat;
	TAILQ_ENTRY(channel) tailq;
};
struct element
{
	uint64_t chunk_size;
	uint64_t buffer_size;
};
static TAILQ_HEAD(,channel) user_channel;

static bool 
probe_cb(void *cb_ctx, struct spdk_pci_device *pci_dev)
{	
	// printf("Found matching device at %04x:%02x:%02x.%x,vendor:0x%04x,device:0x%04x,socket:%d\n",
	//        spdk_pci_device_get_domain(pci_dev),
	//        spdk_pci_device_get_bus(pci_dev), spdk_pci_device_get_dev(pci_dev),
	//        spdk_pci_device_get_func(pci_dev),
	//        spdk_pci_device_get_vendor_id(pci_dev), spdk_pci_device_get_device_id(pci_dev),
	// 	pci_dev->socket_id);
	 return true;
}
static void
attach_cb(void *cb_ctx, struct spdk_pci_device *pci_dev, struct spdk_ioat_chan *chan)
{
	struct channel *temp;
	temp=spdk_dma_zmalloc(sizeof(*temp), 0, NULL);
	if (temp == NULL) {
		printf("Failed to allocate device struct\n");
		return;
	}
	temp->ioat=chan;
	TAILQ_INSERT_TAIL(&user_channel, temp, tailq);
	// printf("attached channel 0x%x\n",temp);
}
static int
ioat_init(void)
{
	if (spdk_ioat_probe(NULL, probe_cb, attach_cb) != 0) {
		fprintf(stderr, "ioat_probe() failed\n");
		return -1;
	}
	return 0;
}
uint64_t count=0;
uint64_t int_pow(uint64_t base, uint8_t exp)
{
    uint64_t value = 1;
    for (int i = 0; i < exp; ++i)
        value *= base;
    return value;
}
struct ioat_task
{
	uint64_t *src;
	uint64_t *dst;
	void *pool;
	uint64_t size; 
};
struct result
{
	uint64_t time;
	double speed;
};
static void done(void *arg)
{
	struct ioat_task *temp=(struct ioat_task*)arg;
	
	if(memcmp(temp->src, temp->dst, temp->size)==0)
	{
			++count;
			// printf("success!%d\n",count-1);
		// printf("%lx %lx\n",*(uint64_t *)(temp->src),*(uint64_t *)(temp->dst));
	}
	else printf("fail \n");
	spdk_mempool_put(temp->pool,temp->src);
	spdk_mempool_put(temp->pool,temp->dst);
}
static int 
opts_init_env(void)
{
	struct spdk_env_opts opts;
	spdk_env_opts_init(&opts);
	opts.name="test";
	opts.core_mask="0x1";
	if (spdk_env_init(&opts) < 0)return -1;
	TAILQ_INIT(&user_channel);
	return 0;
}
void display_header()
{
    // printf("\n");
    // printf("|-----------------------|---------------------------|--------------------------------------|-------------------------------------------|\n");
    // printf("| Chunk     / Buffer    | Memcpy                    | SPDK (sequential)                    | SPDK (n tasks submitted at once)          |\n");
    // printf("| size      / Size      | Time        | Speed       | Time        | Speed       | Improv.  | n  | Time        | Speed       | Improv.  |\n");
    // printf("|-----------------------|-------------|-------------|-------------|-------------|----------|----|-------------|-------------|----------|\n");
    printf("\n");
    printf("|-----------------------|----------------------------|-------------------------------------------|\n");
    printf("| Chunk     / Buffer    | Memcpy                     | SPDK (n tasks submitted at once)          |\n");
    printf("| size      / Size      | Time       |   Speed       | n  | Time        | Speed       | Improv.  |\n");
    printf("|-----------------------|------------|---------------|----|-------------|-------------|----------|\n");
}
struct result par_spdk(uint64_t chunk_size,uint64_t buffer_size,struct spdk_ioat_chan* chan)
{
	uint64_t chunk_num=buffer_size/chunk_size;
	// printf("chunk_num=%8u,chunk_size=%8u,buffer_size=%8u\n",chunk_num,chunk_size,buffer_size);
	// int src[64],dst[64];
	void *src=spdk_mempool_create("src",
						   buffer_size, /* src + dst */
						   chunk_size,
						   SPDK_MEMPOOL_DEFAULT_CACHE_SIZE,
						   spdk_env_get_socket_id(spdk_env_get_current_core()));
	void *dst=spdk_mempool_create("dst",
						   buffer_size, /* src + dst */
						   chunk_size,
						   SPDK_MEMPOOL_DEFAULT_CACHE_SIZE,
						   spdk_env_get_socket_id(spdk_env_get_current_core())+1);
	// printf("src=%lx~%lx,dst=%lx~%lx\n",src,src+buffer_size,dst,dst+buffer_size);
	struct ioat_task *ioat_task=spdk_malloc(sizeof(struct ioat_task)*chunk_num, 0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	if (!pool) {
			printf("Could not allocate buffer pool.\n");
			exit(0);
	}
	uint64_t i,n=0;
	struct result temp;
	srand((unsigned) time(NULL));
	
	// printf("\n");
	
	// for(i=0;i<chunk_num;i++)
	// {
	// 	src_num[i]=i*chunk_size;
	// 	dst_num[i]=(chunk_num-1-i)*chunk_size;
	// 	// src_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
	// 	// dst_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
	// 	// printf("%8u %8u %8u\n",src_num[i],dst_num[i],i);  //(rand()%chunk_num)
	// }
	// printf("1\n");

  // for(i=0;i<chunk_num;i++)
	// {
	// 	ioat_task[i].src=src + src_num[i];
	// 	ioat_task[i].dst=dst + dst_num[i];
	// 	ioat_task[i].size=chunk_size;		
	// 	// printf("%lx %lx\n",ioat_task[i].src,ioat_task[i].dst);
	// 	// printf("%lx %lx\n",*ioat_task[i].src,*ioat_task[i].dst);
	// }
		

	// printf("2\n");
	struct timespec start,end; 
	uint64_t timeuse=0;
	
 	do
	{
		count=0;
		
			for(i=0;i<chunk_num;i++)
	{
		// src_num[i]=i*chunk_size;
		// dst_num[i]=(chunk_num-1-i)*chunk_size;
		// src_num[i]=(uint64_t)(rand()%((chunk_num-1)/2))*chunk_size;
		// dst_num[i]=(uint64_t)(rand()%((chunk_num-1)/2))*chunk_size;
		// printf("%8u %8u %8u\n",src_num[i],dst_num[i],i);  //(rand()%chunk_num)
		
		ioat_task[i].src=spdk_mempool_get(src);
		ioat_task[i].dst=spdk_mempool_get(dst);
		// *(ioat_task[i].src)=rand()%int_pow(2,16);
		// *(ioat_task[i].dst)=rand()%int_pow(2,16);
		ioat_task[i].size=chunk_size;
		ioat_task[i].pool=pool;
		// printf("%lx %lx\n",*(ioat_task[i].src),*(ioat_task[i].dst));
	}
		// for(i=0;i<buffer_size/sizeof(uint8_t);i++)
		// {
		// 	src[i]=rand();
		// 	dst[i]=rand();		
		// // printf("%lx %lx\n",src[i],dst[i]);
		// }	
		clock_gettime(CLOCK_REALTIME, &start);
		for (int i = 0; i < chunk_num; ++i)
			{
     	 // Submit 1 copy
        spdk_ioat_submit_copy(
        chan,
         &ioat_task[i],
        done,
        ioat_task[i].src,
        ioat_task[i].dst,
        ioat_task[i].size);
  		}
			 do
    {
    	spdk_ioat_process_events(chan);
    } while (count!=chunk_num);
	clock_gettime(CLOCK_REALTIME, &end);
	n++;
	timeuse=timeuse+(end.tv_sec*int_pow(10,9)+end.tv_nsec)-(start.tv_sec*int_pow(10,9)+start.tv_nsec);
//	printf("timeuse.tv_usec=%luns\n",end.tv_sec);
	// printf("timeuse=%luns\n",timeuse);
	}while(timeuse<1000000000);
	
	// printf("start %llus/%lluns %lluns\n",start.tv_sec,start.tv_sec*int_pow(10,9),start.tv_nsec);
	// printf("end %llus/%lluns %lluns\n",end.tv_sec,end.tv_sec*int_pow(10,9),end.tv_nsec);
	
	// printf("SPEED=%lfMib/s\n",((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9)));
	
	temp.time=timeuse/(n*chunk_num);
	temp.speed=((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9));
	spdk_mempool_free(pool);
	return  temp;
	
// 		if((n=spdk_ioat_submit_copy(chan,arg,done,dst,src,sizeof(int)*chunk_num))!=0)
// 		{
// 			printf("error %d\n",n);
// 			exit(0);
// 		}	
// 	tsc_end = spdk_get_ticks() + spdk_get_ticks_hz();
// 	while(*arg!=true)
// 	{
// 		spdk_ioat_process_events(chan);
// //		printf("polling\n");
// 	}
// 	printf("\n");

	// for(i=0;i<chunk_num;i++)
	// 	{
	// 		printf("%4d ",dst[i]);
	// 		if((i+1)%32==0)printf("\n");
	// 	}
}
struct result seq_memcpy(uint64_t chunk_size,uint64_t buffer_size)
{
	uint64_t chunk_num=buffer_size/chunk_size;
	// printf("chunk_num=%8u,chunk_size=%8u,buffer_size=%8u\n",chunk_num,chunk_size,buffer_size);
	// int src[64],dst[64];
	// uint64_t *src_num=spdk_malloc(sizeof(uint64_t)*chunk_num,0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	// uint64_t *dst_num=spdk_malloc(sizeof(uint64_t)*chunk_num,0, NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	// uint64_t *src_num=malloc(sizeof(uint64_t)*chunk_num);//会挂机
	// uint64_t *dst_num=malloc(sizeof(uint64_t)*chunk_num);//会挂机
	// uint8_t *src=spdk_malloc(buffer_size,sizeof(uint64_t), NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	// uint8_t *dst=spdk_malloc(buffer_size,sizeof(uint64_t), NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	void *pool=spdk_mempool_create("src",
						   chunk_num*2, /* src + dst */
						   chunk_size,
						   SPDK_MEMPOOL_DEFAULT_CACHE_SIZE,
						   SPDK_ENV_SOCKET_ID_ANY);
	// printf("pool=%lx~%lx\n",pool,pool+chunk_num*2);
	struct ioat_task *ioat_task=spdk_malloc(sizeof(struct ioat_task)*chunk_num, sizeof(struct ioat_task), NULL,SPDK_ENV_SOCKET_ID_ANY,SPDK_MALLOC_DMA);
	// printf("ioat_task=%lx~%lx\n",ioat_task,ioat_task+chunk_num);
	uint64_t i,n=0;
	struct result temp;
	// for(i=0;i<chunk_num;i++)
	// {
	// 	src_num[i]=i*chunk_size;
	// 	dst_num[i]=(chunk_num-1-i)*chunk_size;
	// 	// src_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
	// 	// dst_num[i]=(uint64_t)(rand()%chunk_num)*chunk_size;
	// 	// printf("%8u %8u %8u\n",src_num[i],dst_num[i],i);  //(rand()%chunk_num)
	// }
	// printf("1\n");
srand((unsigned) time(NULL));
	// printf("2\n");
	struct timespec start,end; 
	uint64_t timeuse=0;	
 	do
	{
		for(i=0;i<chunk_num;i++)
	{
		// src_num[i]=i*chunk_size;
		// dst_num[i]=(chunk_num-1-i)*chunk_size;
		// src_num[i]=(uint64_t)(rand()%((chunk_num-1)/2))*chunk_size;
		// dst_num[i]=(uint64_t)(rand()%((chunk_num-1)/2))*chunk_size;
		// printf("%8u %8u %8u\n",src_num[i],dst_num[i],i);  //(rand()%chunk_num)
		
		ioat_task[i].src=spdk_mempool_get(pool);		
		ioat_task[i].dst=spdk_mempool_get(pool);
		*(ioat_task[i].src)=rand()%int_pow(2,16);
		*(ioat_task[i].dst)=rand()%int_pow(2,16);
		
		ioat_task[i].size=chunk_size;
		ioat_task[i].pool=pool;
		// printf("%lx %lx\n",(ioat_task[i].src),(ioat_task[i].dst));
	}
		clock_gettime(CLOCK_REALTIME, &start);
		for (int i = 0; i < chunk_num; ++i)
			{
				memcpy(ioat_task[i].src,ioat_task[i].dst,ioat_task[i].size);
  		}
		
		clock_gettime(CLOCK_REALTIME, &end);
		timeuse=timeuse+(end.tv_sec*int_pow(10,9)+end.tv_nsec)-(start.tv_sec*int_pow(10,9)+start.tv_nsec);		
		// printf("2\n");
	
//	printf("timeuse.tv_usec=%luns\n",end.tv_sec);
	// printf("timeuse=%luns\n",timeuse);
	for(i=0;i<chunk_num;i++)
	{
		spdk_mempool_put(pool, ioat_task[i].src);
		spdk_mempool_put(pool, ioat_task[i].dst);
	}
	n++;
	}while(timeuse<1000000000);
	
	// printf("start %llus/%lluns %lluns\n",start.tv_sec,start.tv_sec*int_pow(10,9),start.tv_nsec);
	// printf("end %llus/%lluns %lluns\n",end.tv_sec,end.tv_sec*int_pow(10,9),end.tv_nsec);	
	// printf("SPEED=%lfMib/s\n",((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9)));
	temp.time=timeuse/(n*chunk_num);
	temp.speed=((double)chunk_num*n*chunk_size/(1024*1024))/((double)timeuse/int_pow(10,9));
	spdk_mempool_free(pool);
	spdk_free(ioat_task);
	return  temp;
}
char* Byte_transfer(uint64_t n)
{
	if(n<10)
	{
		return "B";
	}
	if(n>10 && n<20)return "KB";
	else return "MB";
}
uint64_t Byte_to(uint64_t n)
{
	if(n<int_pow(2,10))return n;
	if(n>int_pow(2,10) && n<int_pow(2,20))return n/int_pow(2,10);
	else return n/int_pow(2,20);
}

int main(int argc, char **argv)
{
	if(opts_init_env()<0)
	{
		printf("opts init environment fail.\n");
		return 0;		
	}
	else printf("opts init environmrnt success.\n");	
	if(ioat_init()<0)
	{
		printf("ioat init environment fail.\n");
		return 0;
	}
	else printf("ioat init environment success.\n");
	struct element e[ELE_NUM]={{1, 5},   {3, 5},   {3, 9},   {5, 9},   {7, 9},   {7, 13},  {9, 13},
        {11, 13}, {11, 17}, {13, 17}, {15, 17}, {15, 21}, {17, 21}, {19, 21},
        {19, 25}, {21, 25}, {23, 25}, {23, 29}, {25, 29}, {27, 29},{27, 31},{29, 31},{30, 31}};
	struct result temp1,temp2;
	struct channel *channel=TAILQ_FIRST(&user_channel);
	struct spdk_ioat_chan *chan=channel->ioat;
	display_header();
	for(int i=0;i<ELE_NUM;i++)
	{
		printf("|%7u%2s %8u%2s    ",Byte_to(int_pow(2,e[i].chunk_size)),Byte_transfer(e[i].chunk_size),Byte_to(int_pow(2,e[i].buffer_size)),Byte_transfer(e[i].buffer_size));
		temp1=seq_memcpy(int_pow(2,e[i].chunk_size),int_pow(2,e[i].buffer_size));
		printf("%9uns   %9.3lfMib/s",temp1.time,temp1.speed);
		temp2=par_spdk(int_pow(2,e[i].chunk_size),int_pow(2,e[i].buffer_size),chan);
	 par_spdk(8,32,chan);
		printf("%3d  %9uns    %8.3lfMib/s      %2.2f |\n",int_pow(2,e[i].buffer_size-e[i].chunk_size),temp2.time,temp2.speed,temp2.speed/temp1.speed);
	}
}
// {1, 5},   {3, 5},   {3, 9},   {5, 9},   {7, 9},   {7, 13},  {9, 13},
//         {11, 13}, {11, 17}, {13, 17}, {15, 17}, {15, 21}, {17, 21}, {19, 21},
//         {19, 25}, {21, 25}, {23, 25}, {23, 29}, {25, 29}, {27, 29}
