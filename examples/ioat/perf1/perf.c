#include "spdk/stdinc.h"
#include "spdk/ioat.h"
#include "spdk/env.h"
#include "spdk/queue.h"
#include "spdk/string.h"
#include <time.h>


static TAILQ_HEAD(, spdk_ioat_chan) attached_chans;
// attached_chans = TAILQ_HEAD_INITIALIZER(attached_chans);

static bool
probe_cb(void *cb_ctx, struct spdk_pci_device *pci_dev)
{

	return true;
}
struct spdk_ioat_chan *ioat = NULL;
static void
attach_cb(void *cb_ctx, struct spdk_pci_device *pci_dev, struct spdk_ioat_chan *chan)
{
	// TAILQ_INSERT_TAIL(&attached_chans, chan, tailq);
	if(!ioat)
	ioat = chan;
}

void parse_args(int argc, char **argv, struct spdk_env_opts *opts)
{
	int op;
	while ((op = getopt(argc, argv, "c:hn:o:q:t:v")) != -1)
	{
		switch (op)
		{
		case 'o':
			// user_config.xfer_size_bytes = spdk_strtol(optarg, 10);
			break;
		// case 'n':
		// 	user_config.chan_num = spdk_strtol(optarg, 10);
		// 	break;
		case 'q':
			// user_config.queue_depth = spdk_strtol(optarg, 10);
			break;
		case 't':
			// user_config.time_in_sec = spdk_strtol(optarg, 10);
			break;
		case 'c':
			opts->core_mask = optarg;
			break;
		case 'v':
			// user_config.verify = true;
			break;
			// case 'chan_id':
			// 	user_config.chan_id= spdk_strtol(optarg, 10);
		}
	}
	return 0;
}

static int
opts_init_env(int argc, char **argv)
{
	struct spdk_env_opts *opts = malloc(sizeof(struct spdk_env_opts));
	spdk_env_opts_init(opts);
	parse_args(argc, argv, opts);
	opts->name = "test";
	// opts->core_mask="0x1";
	if (spdk_env_init(opts) < 0)
		return -1;
	if (spdk_ioat_probe(NULL, probe_cb, attach_cb) != 0)
	{
		printf(stderr, "ioat_probe() failed\n");
		return -1;
	}
	// printf("here\n");
	return 0;
}

void callback(void *cb_arg)
{
	int *n = (int *)cb_arg;
	*n = *n + 1;
	printf("n = %d\n", *n);
}

int main(int argc, char **argv)
{
	clock_t start, finish;
	double speed, duration;
	if (opts_init_env(argc, argv) < 0)
	{
		printf("opts init environment fail.\n");
		return 0;
	}
	printf("Getting one channel %lu\n", ioat);
	int size = pow(2,30);
	int *src = spdk_malloc(size, 0 , NULL, spdk_env_get_socket_id(spdk_env_get_current_core()), SPDK_MALLOC_DMA);
	int *dst = spdk_malloc(size, 0 , NULL, spdk_env_get_socket_id(spdk_env_get_current_core())+1, SPDK_MALLOC_DMA);
	int n = 0;
	start = clock();
	spdk_ioat_submit_copy(ioat, &n, callback, dst, src, size);
	while(n != 1)
	{
		spdk_ioat_process_events(ioat);
	}
	finish = clock();
	duration += (double)(finish - start) / CLOCKS_PER_SEC;
	speed = (size/ pow(2,20)) / duration;
	printf("%lf MiB/s\n",speed);
	
	
	
}