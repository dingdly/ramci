ifeq ($(origin CC),default)
CC = cc
endif

ifeq ($(origin CXX),default)
CXX = g++
endif

ifeq ($(origin LD),default)
LD = ld
endif

CCAR=ar
CC_TYPE=gcc
LD_TYPE=bfd
